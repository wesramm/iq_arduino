/**************************************************************************/
/*!
    Valuechain "iQ" Machine Utilization Sensor Arduino Huzzah Featherweight
    Source Code
    
    @author   Wes Ramm 
    @license  All rights reserved, except for shared libraries
    

    @section  HISTORY

   v 1.1 - Ignoring anything to do with Wifi or MQTT now, just get hte main accel
   loop running at 100 Hz to put accel data into arrays.
   This is the first major change in the iQ sensor.  We are removing the DC offset from the data
   so that we are agnostic of orientation for detection.
   The accel is running at approx 100 Hz (adjustable) for 1 second for processing the data
   We keep it running for only 1 second so that we don't fill up the memory since we have 
   to keep the date in an array.
   v1.2 - Reincorporate button state for live adjustment of sensitivity
   v1.3 - Reincorporate WiFi
   v1.4 - Reicorporate MQTT Publishing
   v1.9 - Add long period loop for now so we don't overwhelm the MQTT Server
   Add status word.  Publish GUID.
   v2.0 - All systems go
   v2.1 - Added LED Blip on MQTT loop to show heartbeat
   v2.2 - Prep for AE Pilot.  Now publishing Sensitivity Value to MQTT.
          Connect to Valuechain EE hotspot.
   
   
*/
/**************************************************************************/


/****************Pre-Processor Directives**********************************/

#include <Wire.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>
#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

/************************* WiFi Access Point *********************************/
// Gumby Gumbo used for Valuechain EE Hotspot
//#define WLAN_SSID       "MY WIFI 2A47"
//#define WLAN_PASS       "MYWIFI3203"

//VM used for Wes Home Dev
#define WLAN_SSID       "VM673571-2G"
#define WLAN_PASS       "qpmdyaed"

// AE engineering WIFI
//#define WLAN_SSID       "AE-GUEST"
//#define WLAN_PASS       "5739911120"


/************************* Adafruit.io Setup *********************************/
#define AIO_SERVER      "io.adafruit.com"
#define AIO_SERVERPORT  1883                   // use 8883 for SSL
#define AIO_USERNAME    "wesramm"
#define AIO_KEY         "c8e9a445f6d545379808916e3f45be65"


/************ Global Variable Definitions ******************/
#define SENSOR_GUID "SN001"
#define BUTTON_PIN 16
#define LED_PIN 12
#define MQTT_INTERVAL 5
int iQ_status=0;
const float Vibe_Threshold = 12;

/************ Setup Hardware ******************/

// Instantiate MMA session for Accel
Adafruit_MMA8451 mma = Adafruit_MMA8451();

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// or... use WiFiFlientSecure for SSL
//WiFiClientSecure client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

/*************************  MQTT Feeds ***************************************/

// Setup the feeds needed to broadcast to the ERP

// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
Adafruit_MQTT_Publish MQTT_GUID = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/guid");
Adafruit_MQTT_Publish MQTT_Vibe = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/vibe");
Adafruit_MQTT_Publish MQTT_STATUS = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/iqstatus");
Adafruit_MQTT_Publish MQTT_THRESHOLD = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/threshold");



void setup(void) {

  // initialize the LED pin as an output:
  pinMode(LED_PIN, OUTPUT);

// Give some LED indication that hte system has come to life.

  digitalWrite(LED_PIN, HIGH);
  delay (2000);
  digitalWrite(LED_PIN, LOW);
  
  Serial.begin(9600);

 //Look to see if we can get on the Wifi
  // Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());
  MQTT_STATUS.publish(1);
  

// Look to see if we can start the Accelerometer.  If not....

  if (! mma.begin()) 
    {Serial.println("Couldnt start");}
  else { Serial.println("MMA8451 found!");}
    
  mma.setRange(MMA8451_RANGE_2_G);  

  digitalWrite(LED_PIN, LOW);    

  MQTT_connect();
  MQTT_GUID.publish(SENSOR_GUID);
  Serial.print("GUID Published");
  Serial.println(SENSOR_GUID);
 
  
  }

/************************************************************************************/
/*************              Main Loop       ******************************************/

float sensitivity=1;
unsigned long currentTick, elapsed;
unsigned long previousTick=0;
bool vibe_detected;

void loop() {
    int x,y,z;
    int n, vibe;
    int setting;     
    int X_Array[100];
    int Y_Array[100];
    int Z_Array[100];
    float x_rms, y_rms, z_rms;

    MQTT_connect();
      vibe=0;
      for (n=0;n<=99;n++)
      {
       
        mma.read();
        x=(mma.x); 
        y=(mma.y);
        z=(mma.z);
        X_Array[n]=x;
        Y_Array[n]=y;
        Z_Array[n]=x;
        delay (10);
      }
       x_rms= calculate_RMS(X_Array,n,0);
       y_rms= calculate_RMS(Y_Array,n,0);
       z_rms= calculate_RMS(Y_Array,n,0);
  
       if (x_rms>(Vibe_Threshold*sensitivity)){vibe=vibe+1;}
       if (y_rms>(Vibe_Threshold*sensitivity)){vibe=vibe+2;}
       if (z_rms>(Vibe_Threshold*sensitivity)){vibe=vibe=vibe+4;}
  
       if (vibe>0)
                {Serial.print ("Vibe Detected:  ");Serial.println(vibe);
                 digitalWrite(LED_PIN, HIGH);
                 vibe_detected=(1); // latch any vibe for long loop
                }
        else    {digitalWrite(LED_PIN, LOW);}
       
  
       if (digitalRead(BUTTON_PIN) == HIGH)
            {            
              sensitivity = sensitivity + 0.5;
              Serial.println ("Pin is High.Sensitivity Adjustment.");
              Serial.print ("New Threshold = ");Serial.println(Vibe_Threshold*sensitivity);
              MQTT_THRESHOLD.publish(Vibe_Threshold*sensitivity);
              delay (3000);
            }
  
  /*******************************************************************/
  // Below this line we'll be doing things on a longer timescale.
  // We do want to be looking at the accel on a regular basis, 
  // but we don't want to overload the MQTT server.
  
  // on longer timeout, publish and reset the vibe detected flag  

      currentTick=millis();
      elapsed=currentTick-previousTick;   
      //Serial.print("Elapsed: ");Serial.println(elapsed);  
 
      if (elapsed >= (MQTT_INTERVAL*1000))
     {
            digitalWrite(LED_PIN, HIGH);
            Serial.println("MQTT publishing Loop");
             previousTick=currentTick;
             if (vibe_detected){Serial.println("There was vibe detected in this interval");}      
             vibe_detected = false;
             
           if (! MQTT_Vibe.publish(vibe)) 
                 {
                 Serial.println("Failed");
                  }
              else
                {
                 Serial.println("MQTT Publish OK!");
                }
                delay (250);
                digitalWrite(LED_PIN, LOW);
     }
}




//***********************************************************************
// Subroutines
//***********************************************************************


// Definition of RMS Function for Array

float calculate_RMS(int Array[100],int n,int debug)
    {
      float rms_value;
      int i;
      float sum, ave, rms;
      
// Get Array average

    if (debug!=0){Serial.println("Start of RMS");}
    
    for(n=0;n<=99;n++)
    {
      sum += Array[n];
    }
    ave = sum/n;
 
// Remove DC Offset

    sum=0;
    for(n=0;n<=99;n++)
    {
      Array[n]-= ave;
    }

// Calculate RMS of X Array

 for(n=0;n<=99;n++)
    {
      sum += Array[n]*Array[n];
    }
    rms = sqrt(sum/n);

    if(debug!=0){Serial.print("RMS= ");Serial.println(rms);}

    return rms;
    } 



// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}
