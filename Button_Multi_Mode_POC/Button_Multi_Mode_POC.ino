/**************************************************************************/
/*!
    Valuechain "iQ" Machine Utilization Sensor Arduino Huzzah Featherweight
    Source Code
    
    @author   Wes Ramm 
    @license  All rights reserved, except for shared libraries
    

    @section  HISTORY

*/
/**************************************************************************/


/****************Pre-Processor Directives**********************************/

#include <Wire.h>
#include <Adafruit_Sensor.h>


/************ Global Variable Definitions ******************/
//Hardware Setup Variables.  DO NOT CHANGE
#define BUTTON_PIN 16
#define LED_PIN 12




/* Here is the setup loop to get everything going.  This shouldn't change
 *  
 */
void setup(void) {

  // initialize the LED pin as an output:
  pinMode(LED_PIN, OUTPUT);

// Give some LED indication that hte system has come to life.

  digitalWrite(LED_PIN, HIGH);
  delay (2000);
  digitalWrite(LED_PIN, LOW);
  
  Serial.begin(9600);


  digitalWrite(LED_PIN, LOW);    

   }

/************************************************************************************/
/*************              Main Loop       ******************************************/

unsigned long currentTick;

unsigned long button_tick, button_elapsed;

unsigned long previousTick=0;

void loop() 
{

/* Michael, please update the SN of the sensor here for each new sensor
 *  Later, we'll program the device through a dedicated interface, but for now
 *  each sensor needs its own SN programmed here.  Restricted at this time to 3 digits ONLY
 *  
 */


// Step 1: Gather the data and put into the array for RMS and averaging later

// The CAL Loop will simply be a longer slower loop (1 minute) of gathering data and then latching RMS levels.

                previousTick=millis();
            
                 while (digitalRead(BUTTON_PIN) == HIGH)
                      {            
                        currentTick=millis();
                        button_elapsed=currentTick-previousTick;
                        
                        Serial.print ("Button Pressed for : "); Serial.print ((button_elapsed/1000)); Serial.println("  Seconds");
                        delay (500);
                      }
            
                       Serial.println ("Main Loop = ");

                 if (button_elapsed<=5000){Serial.println("Mode 1");}
                 else {Serial.println("Mode 2");}
                 
                       
                        delay (1000);
 
 
}
