/**************************************************************************/
/*!
    Valuechain "iQ" Machine Utilization Sensor Arduino Huzzah Featherweight
    Source Code
    
    @author   Wes Ramm 
    @license  All rights reserved, except for shared libraries
    

    @section  HISTORY

   v 1.1 - Ignoring anything to do with Wifi or MQTT now, just get hte main accel
   loop running at 100 Hz to put accel data into arrays.
   This is the first major change in the iQ sensor.  We are removing the DC offset from the data
   so that we are agnostic of orientation for detection.
   The accel is running at approx 100 Hz (adjustable) for 1 second for processing the data
   We keep it running for only 1 second so that we don't fill up the memory since we have 
   to keep the date in an array.
   v1.2 - Reincorporate button state for live adjustment of sensitivity
   v1.3 - Reincorporate WiFi
   v1.4 - Reicorporate MQTT Publishing
   v1.9 - Add long period loop for now so we don't overwhelm the MQTT Server
   Add status word.  Publish GUID.
   v2.0 - All systems go
   v2.1 - Added LED Blip on MQTT loop to show heartbeat
   v2.2 - Prep for AE Pilot.  Now publishing Sensitivity Value to MQTT.
          Connect to Valuechain EE hotspot.
   v3.0 - Serialise each sensor, have sensor transmit SN to MQTT Broker
          Change adafruit MQTT logon credentials to VTL
   v 4.0 - Change transmit structure to reduce MQTT traffic -
         We'll only transmit vibe information if vibe has been detected.
         Updated main loop to latch vibe state incase vibe was detected at beginning of samping interval. 
         We still want to report the vibe and don't want to overwrite the status with '000' if no vibe is detected 
         during later samples.   10/4/19 WTR
   v 5.0 - Long loop for transmitting sensor "I am alive".  So even if we aren't transmitting every second, 
          we will still be able to ascertain whether the sensor is up and running and healthy. 
          For now the status word is empty, but we'll use it later to transmit actual sensor healt
          telemetry.  11/4/19 WTR
   
*/
/**************************************************************************/


/****************Pre-Processor Directives**********************************/

#include <Wire.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>
#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

/************************* WiFi Access Point *********************************/
// Gumby Gumbo used for WES DEV

#define WLAN_SSID       "GumbyGumbo"
#define WLAN_PASS       "Monkey98"

/*****************************************************************************/
/* Michael, I've prepacked this code to get the sensors onto the Valuechain
 *  GSM Wifi Router.
 */
//#define WLAN_SSID       "TP-LINK_D931"
//#define WLAN_PASS       "94533873"


/************************* Adafruit.io Setup *********************************/
#define AIO_SERVER      "io.adafruit.com"
#define AIO_SERVERPORT  1883                   // use 8883 for SSL

// Default Adafruit account
// #define AIO_USERNAME    "wesramm"
// #define AIO_KEY         "c8e9a445f6d545379808916e3f45be65"

// VTL Account
#define AIO_USERNAME    "vcadmin"
#define AIO_KEY         "a3f959300ce74ea988a7bddd6059e7a4"


/************ Global Variable Definitions ******************/
//Hardware Setup Variables.  DO NOT CHANGE
#define BUTTON_PIN 16
#define LED_PIN 12

// Variables for publishing data.  These can change.
#define MQTT_INTERVAL 30
#define STATUS_INTERVAL 300


int iQ_status=0;
const float Vibe_Threshold = 12;

/************ Setup Hardware ******************/

// Instantiate MMA session for Accel
Adafruit_MMA8451 mma = Adafruit_MMA8451();

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// or... use WiFiFlientSecure for SSL
//WiFiClientSecure client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

/*************************  MQTT Feeds ***************************************/

// Setup the feeds needed to broadcast to the ERP

// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
Adafruit_MQTT_Publish MQTT_GUID = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/guid");
Adafruit_MQTT_Publish MQTT_Vibe = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/toolvibe");
Adafruit_MQTT_Publish MQTT_STATUS = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/sensorhealth");
Adafruit_MQTT_Publish MQTT_THRESHOLD = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/threshold");

void setup(void) {

  // initialize the LED pin as an output:
  pinMode(LED_PIN, OUTPUT);

// Give some LED indication that hte system has come to life.

  digitalWrite(LED_PIN, HIGH);
  delay (2000);
  digitalWrite(LED_PIN, LOW);
  
  Serial.begin(9600);

 //Look to see if we can get on the Wifi
  // Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());
  MQTT_STATUS.publish(1);
  

// Look to see if we can start the Accelerometer.  If not....

  if (! mma.begin()) 
    {Serial.println("Couldnt start");}
  else { Serial.println("MMA8451 found!");}
    
  mma.setRange(MMA8451_RANGE_2_G);  

  digitalWrite(LED_PIN, LOW);    

   }

/************************************************************************************/
/*************              Main Loop       ******************************************/

float sensitivity=1;
unsigned long currentTick, elapsed;
unsigned long previousTick=0;
unsigned long statusTick, statusElapsed;
unsigned long statusPreviousTick=0;

bool vibe_detected;
char payload;

void loop() {


/* Michael, please update the SN of the sensor here for each new sensor
 *  Later, we'll program the device through a dedicated interface, but for now
 *  each sensor needs its own SN programmed here.  Restricted at this time to 3 digits ONLY
 *  
 */
    char guid[6]="SN011";

    
    int x,y,z;
    int n, vibe, latch;
    int setting;     
    int X_Array[100];
    int Y_Array[100];
    int Z_Array[100];
    float x_rms, y_rms, z_rms;
    char vibestate[4];
    char payload[10];
    char sensorstatus[11];
   
    MQTT_connect();
    vibe=0;
     
      for (n=0;n<=99;n++)
      {
        mma.read();
        x=(mma.x); 
        y=(mma.y);
        z=(mma.z);
        X_Array[n]=x;
        Y_Array[n]=y;
        Z_Array[n]=x;
        delay (10);
      }
       x_rms= calculate_RMS(X_Array,n,0);
       y_rms= calculate_RMS(Y_Array,n,0);
       z_rms= calculate_RMS(Y_Array,n,0);
  
       if (x_rms>(Vibe_Threshold*sensitivity)){vibe=vibe+1;}
       if (y_rms>(Vibe_Threshold*sensitivity)){vibe=vibe+2;}
       if (z_rms>(Vibe_Threshold*sensitivity)){vibe=vibe+4;}
// This stuff here is for dev purposes only to make the IO stream human-readable.
// The last state of the vibe will be latched here so that the reporting to the MQTT server is accurate.
// This means that we will NOT reinitialise the string here to 000        
       if (vibe==1){strcpy(vibestate,"_X00");}
       if (vibe==2){strcpy(vibestate,"_0Y0");}
       if (vibe==3){strcpy(vibestate,"_XY0");}
       if (vibe==4){strcpy(vibestate,"_00Z");}
       if (vibe==5){strcpy(vibestate,"_X0Z");}
       if (vibe==6){strcpy(vibestate,"_0YZ");}
       if (vibe==7){strcpy(vibestate,"_XYZ");}
       Serial.print("vibestate=");Serial.println(vibestate);
  
       if (vibe>0)
                {Serial.print ("Vibe Detected:  ");Serial.println(vibe);
                 digitalWrite(LED_PIN, HIGH);
                 vibe_detected=(1); // latch any vibe for long loop
                }
        else    {digitalWrite(LED_PIN, LOW);}
  
       if (digitalRead(BUTTON_PIN) == HIGH)
            {            
              sensitivity = sensitivity + 0.5;
              Serial.println ("Pin is High.Sensitivity Adjustment.");
              Serial.print ("New Threshold = ");Serial.println(Vibe_Threshold*sensitivity);
              MQTT_THRESHOLD.publish(Vibe_Threshold*sensitivity);
              delay (3000);
            }
  
  /*******************************************************************/
  // Below this line we'll be doing things on a longer timescale.
  // We do want to be looking at the accel on a regular basis, 
  // but we don't want to overload the MQTT server.

//  unsigned long statusTick, StatusElapsed;
//  unsigned long statusPreviousTick=0;
  
  // on longer timeout, publish and reset the vibe detected flag  

// current tick is the universal timer element.  Can be used for both timers.

      currentTick=millis();
      
      elapsed=currentTick-previousTick;   
      
      //Serial.print("Elapsed: ");Serial.println(elapsed);  
 
      if (elapsed >= (MQTT_INTERVAL*1000))
     {
            digitalWrite(LED_PIN, HIGH);
            Serial.println("MQTT publishing Loop");
             previousTick=currentTick;
             if (vibe_detected)
                 {
                  Serial.println("There was vibe detected in this interval");      
                  strcpy(payload,guid);
                  strcat(payload, vibestate);
                  Serial.print("MQTT PACKET PREVIEW:"); Serial.println(payload);
  // We'll try to publish the MQTT packet now since there was vibe_detected.
                  if (!MQTT_Vibe.publish(payload)){Serial.println("Failed");}
                  else{Serial.println("MQTT Publish OK!");}
                  delay (250);
                  digitalWrite(LED_PIN, LOW);
                  vibe_detected = 0;
                }
            else
                {Serial.println("MQTT Loop finished without publish: No Vibe Detected");}
            
            strcpy(vibestate,"_000");

            statusElapsed=currentTick-statusPreviousTick;

            if (statusElapsed >= (STATUS_INTERVAL*1000))
            {statusPreviousTick=currentTick;
            strcpy(sensorstatus,guid);
            strcat(sensorstatus,"_LIVE");
            MQTT_STATUS.publish(sensorstatus);
            Serial.print("Sensor Status Payload");Serial.println(sensorstatus);
            }
      }
}




//***********************************************************************
// Subroutines
//***********************************************************************


// Definition of RMS Function for Array

float calculate_RMS(int Array[100],int n,int debug)
    {
      float rms_value;
      int i;
      float sum, ave, rms;
      
// Get Array average

    if (debug!=0){Serial.println("Start of RMS");}
    
    for(n=0;n<=99;n++)
    {
      sum += Array[n];
    }
    ave = sum/n;
 
// Remove DC Offset

    sum=0;
    for(n=0;n<=99;n++)
    {
      Array[n]-= ave;
    }

// Calculate RMS of X Array

 for(n=0;n<=99;n++)
    {
      sum += Array[n]*Array[n];
    }
    rms = sqrt(sum/n);

    if(debug!=0){Serial.print("RMS= ");Serial.println(rms);}

    return rms;
    } 



// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}
